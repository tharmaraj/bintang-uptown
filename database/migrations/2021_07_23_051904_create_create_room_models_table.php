<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCreateRoomModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_room_models', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('noofpax');
            $table->integer('noofadult');
            $table->integer('roomno');
            $table->longtext('photo1')->nullable();
            $table->longtext('photo2')->nullable();
            $table->double('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_room_models');
    }
}
