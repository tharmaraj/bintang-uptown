<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomBookingModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_booking_models', function (Blueprint $table) {
            $table->id();
            $table->string('userid');
            $table->integer('roomid');
            $table->integer('noofpax');
            $table->integer('noofadult');
            $table->string('datein');
            $table->string('dateout');
            $table->integer('paymentstatus')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room_booking_models');
    }
}
