<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);

        DB::table('users')->insert([
            'userid' => rand(),
            'name' => 'Tharmaraj',
            'email' => 'tharmaraj123@yahoo.com',
            'password' => Hash::make('123123123'),
            'role' => '1',
        ]);
    }
}
