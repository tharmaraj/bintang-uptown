<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class CustomerController extends Controller
{
    //
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $results = User::all();
        return view('customer/view',compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('customer/store');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $results = new User;

        $results->name = $request->name;
        $results->email = $request->email;
        $results->password = Hash::make($request->password);
        $results->homeadress = $request->homeadress;
        $results->noTel = $request->noTel;
        $results->noIC = $request->noIC;

        $results->save();

        return redirect('customerview');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $results = User::find($id);

        return view('customer/edit',compact('results'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $results = User::find($id);

        $results->name = $request->name;
        $results->email = $request->email;
        $results->password = Hash::make($request->password);
        $results->homeadress = $request->homeadress;
        $results->noTel = $request->noTel;
        $results->noIC = $request->noIC;

        $results->save();

        return redirect('customerview');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $results = User::find($id);
        $results->delete();

        return redirect('customerview');
    }
}
