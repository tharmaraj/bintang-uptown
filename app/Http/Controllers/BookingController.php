<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RoomBookingModel;

class BookingController extends Controller
{
    //
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // $results = RoomBookingModel::all();

        $results = RoomBookingModel::join('create_room_models', 'create_room_models.id','=','room_booking_models.roomid')->
        get();
        
        return view('booking/view',compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('booking/store');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $results = new RoomBookingModel;

        $results->userid = $request->userid;
        $results->roomid = $request->roomid;
        $results->noofpax = $request->noofpax;
        $results->noofadult = $request->noofadult;
        $results->datein = $request->datein;
        $results->dateout = $request->dateout;
        $results->paymentstatus = '0';

        $results->save();

        return redirect('bookingview');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $results = RoomBookingModel::find($id);

        return view('booking/edit',compact('results'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $results = RoomBookingModel::find($id);

        $results->userid = $request->userid;
        $results->roomid = $request->roomid;
        $results->noofpax = $request->noofpax;
        $results->noofadult = $request->noofadult;
        $results->datein = $request->datein;
        $results->dateout = $request->dateout;
        $results->paymentstatus = '1';

        $results->save();

        return redirect('bookingview');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $results = RoomBookingModel::find($id);
        $results->delete();

        return redirect('bookingview');
    }
}
