<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RoomBookingModel;
use App\CreateRoomModel;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    
    public function welcome()
    {   
        $rooms = CreateRoomModel::all();
        return view('welcome', compact('rooms'));
    }
    public function roombooking($id)
    {   
        // dd($id);
        $roomid = $id;
        
        $roommodel = CreateRoomModel::find($roomid);

        $userid = Auth::id();
        return view('roombooking', compact('roomid','userid','roommodel'));
    }

}
