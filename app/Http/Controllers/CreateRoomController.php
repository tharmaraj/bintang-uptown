<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CreateRoomModel;
use URL;

class CreateRoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $results = CreateRoomModel::all();
        return view('rooms/view',compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('rooms/store');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $results = new CreateRoomModel;

        $results->name = $request->name;
        $results->noofpax = $request->noofpax;
        $results->noofadult = $request->noofadult;
        $results->roomno = $request->roomno;
        $results->price = $request->price;
        
        if($request->file('photo1')){
            $new_name = rand().'.'.$request->file('photo1')->getClientOriginalExtension();
            $request->file('photo1')->move(public_path("files"),$new_name);
        }else{
            $new_name = '';
        }
        $results->photo1 = $new_name;
        
        if($request->file('photo2')){
            $new_name2 = rand().'.'.$request->file('photo2')->getClientOriginalExtension();
            $request->file('photo2')->move(public_path("files"),$new_name2);
        }else{
            $new_name2 = '';
        }
        $results->photo2 = $new_name2;

        $results->save();

        return redirect('roomsview');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $results = CreateRoomModel::find($id);

        return view('rooms/edit',compact('results'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $results = CreateRoomModel::find($id);

        $results->name = $request->name;
        $results->noofpax = $request->noofpax;
        $results->noofadult = $request->noofadult;
        $results->roomno = $request->roomno;
        $results->price = $request->price;


        if($request->file('photo1')){
            // if(file_exists(Request::url().'/files/'.$request->photo1)){
            //     unlink('files/'.$request->photo1);
            // }
            $new_name = rand().'.'.$request->file('photo1')->getClientOriginalExtension();
            $request->file('photo1')->move(public_path("files"),$new_name);
        }else{
            $new_name = $request->photo1;
        }

        $results->photo1 = $new_name;
        
        if($request->file('photo2')){
            // if(file_exists(Request::url().'/files/'.$request->photo2)){
            //     unlink('files/'.$request->photo2);
            // }
            $new_name2 = rand().'.'.$request->file('photo2')->getClientOriginalExtension();
            $request->file('photo2')->move(public_path("files"),$new_name2);
        }else{
            $new_name2 = $request->photo2;
        }

        $results->photo2 = $new_name2;

        $results->save();

        return redirect('roomsview');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $results = CreateRoomModel::find($id);
        $results->delete();

        return redirect('roomsview');
    }
}
