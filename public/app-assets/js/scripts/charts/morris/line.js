/*=========================================================================================
    File Name: line.js
    Description: Morris line chart
    ----------------------------------------------------------------------------------------
    Item Name: Stack - Responsive Admin Theme
    Version: 2.1
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

// Line chart
// ------------------------------
$(window).on("load", function(){

    Morris.Line({
        element: 'line-chart',
        data: [{
            "year": "2010",
            "Open": 100,
            "Pending": 40,
            "Closed": 62
        }, {
            "year": "2011",
            "Open": 150,
            "Pending": 200,
            "Closed": 120
        }, {
            "year": "2012",
            "Open": 175,
            "Pending": 105,
            "Closed": 80
        }, {
            "year": "2013",
            "Open": 125,
            "Pending": 150,
            "Closed": 75
        }, {
            "year": "2014",
            "Open": 150,
            "Pending": 275,
            "Closed": 100
        }, {
            "year": "2015",
            "Open": 200,
            "Pending": 325,
            "Closed": 80
        }, {
            "year": "2016",
            "Open": 260,
            "Pending": 130,
            "Closed": 90
        }],
        xkey: 'year',
        ykeys: ['Open', 'Pending', 'Closed'],
        labels: ['Open', 'Pending', 'Closed'],
        resize: true,
        smooth: false,
        pointSize: 3,
        pointStrokeColors:['#00A5A8', '#FF7D4D','#FF4558'],
        gridLineColor: '#e3e3e3',
        behaveLikeLine: true,
        numLines: 6,
        gridtextSize: 14,
        lineWidth: 3,
        hideHover: 'auto',
        lineColors: ['#00A5A8', '#FF7D4D','#FF4558']
    });
});