@extends('layouts.app')

@section('content')
<div class="container">
Rooms
<br>
<a href="{{route('roomcreate')}}">Create New</a>

<table class="table table-border">
    @forelse($results as $result)
    <tr>
        <td>{{$result}}</td>
        <td><img src='files/{{$result->photo1}}' width="100"></td>
        <td><a href="{{ route('roomedit', $result->id ) }}">Edit</a></td>
        <td><a href="{{ route('roomdelete', $result->id ) }}">Delete</a></td>
    </tr>
    @empty
        {{__('No Record')}}
    @endforelse
</table>


</div>

@endsection