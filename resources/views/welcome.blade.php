<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>BINTANG UPTOWN HOTEL BOOKING ONLINE SYSTEM</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Template by FREEHTML5.CO" />
	<meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
	<meta name="author" content="FREEHTML5.CO" />

  <!-- 
	//////////////////////////////////////////////////////

	FREE HTML5 TEMPLATE 
	DESIGNED & DEVELOPED by FREEHTML5.CO
		
	Website: 		http://freehtml5.co/
	Email: 			info@freehtml5.co
	Twitter: 		http://twitter.com/fh5co
	Facebook: 		https://www.facebook.com/fh5co

	//////////////////////////////////////////////////////
	 -->

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">
	<!-- <link href='https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700italic,900,700,900italic' rel='stylesheet' type='text/css'> -->

	<!-- Stylesheets -->
	<!-- Dropdown Menu -->
	<link rel="stylesheet" href="{{ asset('buh_website/css/superfish.css') }}">
	<!-- Owl Slider -->
	<!-- <link rel="stylesheet" href="css/owl.carousel.css"> -->
	<!-- <link rel="stylesheet" href="css/owl.theme.default.min.css"> -->
	<!-- Date Picker -->
	<link rel="stylesheet" href="{{ asset('buh_website/css/bootstrap-datepicker.min.css') }}">
	<!-- CS Select -->
	<link rel="stylesheet" href="{{ asset('buh_website/css/cs-select.css') }}">
	<link rel="stylesheet" href="{{ asset('buh_website/css/cs-skin-border.css') }}">

	<!-- Themify Icons -->
	<link rel="stylesheet" href="{{ asset('buh_website/css/themify-icons.css') }}">
	<!-- Flat Icon -->
	<link rel="stylesheet" href="{{ asset('buh_website/css/flaticon.css') }}">
	<!-- Icomoon -->
	<link rel="stylesheet" href="{{ asset('buh_website/css/icomoon.css') }}">
	<!-- Flexslider  -->
	<link rel="stylesheet" href="{{ asset('buh_website/css/flexslider.css') }}">
	
	<!-- Style -->
	<link rel="stylesheet" href="{{ asset('buh_website/css/style.css') }}">

	<!-- Modernizr JS -->
	<script src="{{ asset('buh_website/js/modernizr-2.6.2.min.js') }}"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>
<body>
	<div id="fh5co-wrapper">
	<div id="fh5co-page">
	<div id="fh5co-header">
		<header id="fh5co-header-section">
			<div class="container">
				<div class="nav-header">
					<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
					<h1 id="fh5co-logo"><a href="/">BINTANG UPTOWN HOTEL</a></h1>
					<nav id="fh5co-menu-wrap" role="navigation">
						<ul class="sf-menu" id="fh5co-primary-menu">
							<li><a class="active" href="/">Home</a></li>
							<li><a href="/gallery">Room Gallery</a></li>
							<li><a href="/contact">Contact</a></li>
							<li><a href="/login">Login</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</header>
		
	</div>
	<!-- end:fh5co-header -->
	<aside id="fh5co-hero" class="js-fullheight">
		<div class="flexslider js-fullheight">
			<ul class="slides">
		   	<li style="background-image: url('https://cdn.suwalls.com/wallpapers/photography/hotel-room-27502-1920x1200.jpg');">
		   	<!-- <li style="background-image: url(buh_website/images/slider1.jpg);"> -->
		   		<div class="overlay-gradient"></div>
		   		<div class="container">
		   			<div class="col-md-12 col-md-offset-0 text-center slider-text">
		   				<div class="slider-text-inner js-fullheight">
		   					<div class="desc">
		   						<p><span>BINTANG UPTOWN HOTEL</span></p>
		   						<h2>Reserve Room for Family Vacation</h2>
			   					<p>
			   						<a href="/booking" class="btn btn-primary btn-lg">Book Now</a>
			   					</p>
		   					</div>
		   				</div>
		   			</div>
		   		</div>
		   	</li>
		   	<li style="background-image: url('https://cdn.wallpaper.com/main/2018/07/1_santa_clara_lisbon.jpg');">
		   	<!-- <li style="background-image: url(buh_website/images/slider2.jpg);"> -->
		   		<div class="overlay-gradient"></div>
		   		<div class="container">
		   			<div class="col-md-12 col-md-offset-0 text-center slider-text">
		   				<div class="slider-text-inner js-fullheight">
		   					<div class="desc">
		   						<p><span>Deluxe Hotel</span></p>
		   						<h2>Make Your Vacation Comfortable</h2>
			   					<p>
			   						<a href="/booking" class="btn btn-primary btn-lg">Book Now</a>
			   					</p>
		   					</div>
		   				</div>
		   			</div>
		   		</div>
		   	</li>
		   	<li style="background-image: url('https://cf.bstatic.com/images/hotel/max1024x768/160/160769368.jpg');">
		   	<!-- <li style="background-image: url(buh_website/images/slider3.jpg);"> -->
		   		<div class="overlay-gradient"></div>
		   		<div class="container">
		   			<div class="col-md-12 col-md-offset-0 text-center slider-text">
		   				<div class="slider-text-inner js-fullheight">
		   					<div class="desc">
		   						<p><span>Luxe Hotel</span></p>
		   						<h2>A Best Place To Enjoy Your Life</h2>
			   					<p>
			   						<a href="/booking/1" class="btn btn-primary btn-lg">Book Now</a>
			   					</p>
		   					</div>
		   				</div>
		   			</div>
		   		</div>
		   	</li>
		   	
		  	</ul>
	  	</div>
	</aside>
	<div class="wrap">
		<div class="container">
			<div class="row">
				<div id="availability">
					<form action="#">

						<div class="a-col">
							<section>
								<select class="cs-select cs-skin-border">
									<option value="" disabled selected>Select Hotel</option>
									<option value="email">Luxe Hotel</option>
									<option value="twitter">Deluxe Hotel</option>
									<option value="linkedin">Five Star Hotel</option>
								</select>
							</section>
						</div>
						<div class="a-col alternate">
							<div class="input-field">
								<label for="date-start">Check In</label>
								<input type="text" class="form-control" id="date-start" />
							</div>
						</div>
						<div class="a-col alternate">
							<div class="input-field">
								<label for="date-end">Check Out</label>
								<input type="text" class="form-control" id="date-end" />
							</div>
						</div>
						<div class="a-col action">
							<a href="#">
								<span>Check</span>
								Availability
							</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	
				
				
			</div>
		</div>
	</div>

	<div id="featured-hotel" class="fh5co-bg-color">
		<div class="container">
			
			<div class="row">
				<div class="col-md-12">
					<div class="section-title text-center">
						<h2>Featured Hotels</h2>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="feature-full-1col">
					<div class="image" style="background-image: url(image/Deluxeroom.jpg);">
						<div class="descrip text-center">
							<p><small>For as low as</small><span>RM250/night</span></p>
						</div>
					</div>
					<div class="desc">
						<h3>Deluxe Room</h3>
						<p>Deluxe room equipped with shower room, Fridge, TV, Sofa,  Windows Side corner room  </p>
						<p> 4 Bed Double Queen Suit For 8 Pax</p>
						<p><a href="/booking/1" class="btn btn-primary btn-luxe-primary">Book Now <i class="ti-angle-right"></i></a></p>
					</div>
				</div>
				
				<div class="feature-full-2col">
					<div class="f-hotel">
						<div class="image" style="background-image: url(image/Mediumroom.jpg);">
							<div class="descrip text-center">
								<p><small>For as low as</small><span>RM150/night</span></p>
							</div>
						</div>
						<div class="desc">
							<h3>Medium Room</h3>
							<p> Medium Room Equipped with shower Room, TV, Sofa  </p>
							<p> 2 Bed Double Size suit for 6 Pax </p>
							<p><a href="/booking/2" class="btn btn-primary btn-luxe-primary">Book Now <i class="ti-angle-right"></i></a></p>
						</div>
					</div>
					<div class="f-hotel">
						<div class="image" style="background-image: url(image/BudgetRoom.jpg);">
							<div class="descrip text-center">
								<p><small>For as low as</small><span>RM59/night</span></p>
							</div>
						</div>
						<div class="desc">
							<h3>Budget Room</h3>
							<p>Budge Room only Equipped with Shower Room only</p>
							<p>2 Single Bed suit for 3 Pax</p>
							<p><a href="/booking/3" class="btn btn-primary btn-luxe-primary">Book Now <i class="ti-angle-right"></i></a></p>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

	<div id="hotel-facilities">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="section-title text-center">
						<h2>Hotel Facilities</h2>
					</div>
				</div>
			</div>

			<div id="tabs">
				<nav class="tabs-nav">
					<a href="#" class="active" data-tab="tab1">
						<i class="flaticon-restaurant icon"></i>
						
						<span>Bar & Club House</span>
					</a>
					<a href="#" data-tab="tab2">
						<i class="flaticon-cup icon"></i>
						<span>Living Room</span>
					</a>
					<a href="#" data-tab="tab4">
						
						<i class="flaticon-swimming icon"></i>
						<span>Swimming Pool</span>
					</a>
					<a href="#" data-tab="tab5">
						
						<i class="flaticon-massage icon"></i>
						<span>Spa House</span>
					</a>
					<a href="#" data-tab="tab6">
						
						<i class="flaticon-bicycle icon"></i>
						<span>Gym</span>
					</a>
				</nav>
				<div class="tab-content-container">
					<div class="tab-content active show" data-tab-content="tab1">
						<div class="container">
							<div class="row">
								<div class="col-md-6">
									<img src="image/barclub.jpg" class="img-responsive" alt="Image">
								</div>
								<div class="col-md-6">
									<span class="super-heading-sm">World Class</span>
									<h3 class="heading">Bar & Club</h3>
									<p>This facilies only available at night and customer can having dinner serve with drinks</p>
									<p class="service-hour">
										<span>Operation Hours</span>
										<strong>8:00 PM - 2:00 AM</strong>
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-content" data-tab-content="tab2">
						<div class="container">
							<div class="row">
								<div class="col-md-6">
									<img src="image/livingarea.jpg" class="img-responsive" alt="Image">
								</div>
								<div class="col-md-6">
									<span class="super-heading-sm">World Class</span>
									<h3 class="heading">Living Room</h3>
									<p>Big hall living area and store with small library books & Magazine for customer</p>
									<p class="service-hour">
										<span>Operation Hours</span>
										<strong>7:30 AM - 8:00 PM</strong>
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-content" data-tab-content="tab3">
						<div class="container">
							<div class="row">
								<div class="col-md-6">
									<img src="buh_website/images/tab_img_3.jpg" class="img-responsive" alt="Image">
								</div>
								<div class="col-md-6">
									<span class="super-heading-sm">World Class</span>
									<h3 class="heading">Pick Up</h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias officia perferendis modi impedit, rem quasi veritatis. Consectetur obcaecati incidunt, quae rerum, accusamus sapiente fuga vero at. Quia, labore, reprehenderit illum dolorem quae facilis reiciendis quas similique totam sequi ducimus temporibus ex nemo, omnis perferendis earum fugit impedit molestias animi vitae.</p>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam neque blanditiis eveniet nesciunt, beatae similique doloribus, ex impedit rem officiis placeat dignissimos molestias temporibus, in! Minima quod, consequatur neque aliquam.</p>
									<p class="service-hour">
										<span>Service Hours</span>
										<strong>7:30 AM - 8:00 PM</strong>
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-content" data-tab-content="tab4">
						<div class="container">
							<div class="row">
								<div class="col-md-6">
									<img src="image/swimingpool.jpg" class="img-responsive" alt="Image">
								</div>
								<div class="col-md-6">
									<span class="super-heading-sm">World Class</span>
									<h3 class="heading">Swimming Pool</h3>
									<p>Swimming pool for customer taking Deluxe Room max up to 5 days</p>
									<p>Swimming pool for customer not take Deluxe package but can enjoying with paid RM5 per person</p>
									<p class="service-hour">
										<span>Service Hours</span>
										<strong>10:00 AM - 7:00 PM</strong>
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-content" data-tab-content="tab5">
						<div class="container">
							<div class="row">
								<div class="col-md-6">
									<img src="image/spa.jpg" class="img-responsive" alt="Image">
								</div>
								<div class="col-md-6">
									<span class="super-heading-sm">World Class</span>
									<h3 class="heading">Spa House</h3>
									<p>Relaxing and flexi place for Deluxe customer</p>
									<p>Relaxing and flexi place for not Deluxe customer but can enjoying with paid RM10 per person</p>
									<p class="service-hour">
										<span>Service Hours</span>
										<strong>7:30 AM - 8:00 PM</strong>
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-content" data-tab-content="tab6">
						<div class="container">
							<div class="row">
								<div class="col-md-6">
									<img src="image/gym.jpg" class="img-responsive" alt="Image">
								</div>
								<div class="col-md-6">
									<span class="super-heading-sm">World Class</span>
									<h3 class="heading">Gym</h3>
									<p>Gym area for Deluxe Customer</p>
									<p>Gym area for not Deluxe Customer can enjoy with paid RM3 per person</p>
									<p class="service-hour">
										<span>Service Hours</span>
										<strong>7:30 AM - 8:00 PM</strong>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	
					</div>
				</div>
			</div>
		</div>
	</div>


	<br /> <br /><br /><br />
				</div>
			</div>
		</div>
	</div>

	
						
							
			</div>
		</div>
	</footer>

	</div>
	<!-- END fh5co-page -->

	</div>
	<!-- END fh5co-wrapper -->
	
	<!-- Javascripts -->
	<script src="{{ asset('buh_website/js/jquery-2.1.4.min.js') }}"></script>
	<!-- Dropdown Menu -->
	<script src="{{ asset('buh_website/js/hoverIntent.js') }}"></script>
	<script src="{{ asset('buh_website/js/superfish.js') }}"></script>
	<!-- Bootstrap -->
	<script src="{{ asset('buh_website/js/bootstrap.min.js') }}"></script>
	<!-- Waypoints -->
	<script src="{{ asset('buh_website/js/jquery.waypoints.min.js') }}"></script>
	<!-- Counters -->
	<script src="{{ asset('buh_website/js/jquery.countTo.js') }}"></script>
	<!-- Stellar Parallax -->
	<script src="{{ asset('buh_website/js/jquery.stellar.min.js') }}"></script>
	<!-- Owl Slider -->
	<!-- // <script src="js/owl.carousel.min.js"></script> -->
	<!-- Date Picker -->
	<script src="{{ asset('buh_website/js/bootstrap-datepicker.min.js') }}"></script>
	<!-- CS Select -->
	<script src="{{ asset('buh_website/js/classie.js') }}"></script>
	<script src="{{ asset('buh_website/js/selectFx.js') }}"></script>
	<!-- Flexslider -->
	<script src="{{ asset('buh_website/js/jquery.flexslider-min.js') }}"></script>

	<script src="{{ asset('buh_website/js/custom.js') }}"></script>

</body>
</html>