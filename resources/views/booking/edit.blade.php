@extends('layouts.app')

@section('content')
<div class="container">
Create
<form method="post" action="{{ route('bookingupdate',$results->id) }}" enctype="multipart/form-data">
@csrf

<input type="text" name="userid" value="{{$results->userid}}" placeholder="userid">
<input type="text" name="roomid" value="{{$results->roomid}}" placeholder="roomid">
<input type="integer" name="noofpax" value="{{$results->noofpax}}" placeholder="noofpax">
<input type="integer" name="noofadult" value="{{$results->noofadult}}" placeholder="noofadult">
<input type="date" name="datein" value="{{$results->datein}}" placeholder="datein">
<input type="date" name="dateout" value="{{$results->dateout}}" placeholder="dateout">


<input type="submit" value="Update">
</form>
</div>

@endsection