@extends('layouts.app')

@section('content')
<div class="container">
Room Booking
<br>
<a href="{{route('bookingcreate')}}">Create New</a>

<table class="table table-border">
    @forelse($results as $result)
    <tr>
        <td>{{$result}}</td>
        <td><a href="{{ route('bookingedit', $result->id ) }}">Edit</a></td>
        <td><a href="{{ route('bookingdelete', $result->id ) }}">Delete</a></td>
    </tr>
    @empty
        {{__('No Record')}}
    @endforelse
</table>


</div>

@endsection