@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Uptown Hotel Bintang</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row">
                    
                    <div class="card-body">
                    <a href="{{route('bookinglist')}}">Booking</a>
                    </div>

                    @if(Auth::user()->role == '1')
                    <div class="card-body">
                    <a href="{{route('customerlist')}}">Customer</a>
                    </div>
                    <div class="card-body">
                    <a href="{{route('roomlist')}}">Room</a>
                    </div>
                    @endif
                    
                    </div>



                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
