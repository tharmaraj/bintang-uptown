@extends('layouts.app')

@section('content')
<div class="container">
Rooms
<br>
<a href="{{route('customercreate')}}">Create New</a>

<table class="table table-border">
    @forelse($results as $result)
    <tr>
        <td>{{$result}}</td>
        <td><a href="{{ route('customeredit', $result->id ) }}">Edit</a></td>
        <td><a href="{{ route('customerdelete', $result->id ) }}">Delete</a></td>
    </tr>
    @empty
        {{__('No Record')}}
    @endforelse
</table>


</div>

@endsection