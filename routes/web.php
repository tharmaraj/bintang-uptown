<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', 'HomeController@welcome')->name('welcome');


Route::get('/contact', function () {
    return view('contact');
});
Route::get('/gallery', function () {
    return view('gallery');
});
// Route::get('/booking/{id}', function () {
//     return view('roombooking');
// });

Route::get('/booking/{id}', 'HomeController@roombooking');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/roomsview', 'CreateRoomController@index')->name('roomlist');
Route::get('/roomscreate', 'CreateRoomController@create')->name('roomcreate');
Route::post('/roomsstore', 'CreateRoomController@store')->name('roomstore');
Route::get('/roomsedit/{id}', 'CreateRoomController@edit')->name('roomedit');
Route::post('/roomsupdate/{id}', 'CreateRoomController@update')->name('roomsupdate');
Route::get('/roomsdelete/{id}', 'CreateRoomController@destroy')->name('roomdelete');

Route::get('/customerview', 'CustomerController@index')->name('customerlist');
Route::get('/customercreate', 'CustomerController@create')->name('customercreate');
Route::post('/customerstore', 'CustomerController@store')->name('customerstore');
Route::get('/customeredit/{id}', 'CustomerController@edit')->name('customeredit');
Route::post('/customerupdate/{id}', 'CustomerController@update')->name('customerupdate');
Route::get('/customerdelete/{id}', 'CustomerController@destroy')->name('customerdelete');

Route::get('/bookingview', 'BookingController@index')->name('bookinglist');
Route::get('/bookingcreate', 'BookingController@create')->name('bookingcreate');
Route::post('/bookingstore', 'BookingController@store')->name('bookingstore');
Route::get('/bookingedit/{id}', 'BookingController@edit')->name('bookingedit');
Route::post('/bookingupdate/{id}', 'BookingController@update')->name('bookingupdate');
Route::get('/bookingdelete/{id}', 'BookingController@destroy')->name('bookingdelete');
